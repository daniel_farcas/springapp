package org.springweb.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springweb.scan.services.IHomeService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:org/springweb/test/spring/application-context-test.xml",
		"classpath:org/springweb/test/spring/mvc-context-test.xml" })
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class HomeControllerTest {

	@Test
	public void testUsuarios() {

		Assert.assertFalse(homeService.getUsuarios().isEmpty());
	}

	@Autowired
	private IHomeService homeService;
}
