<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="klum" class="jumbotron">
<p>Formulário</p>
<div class="form-content">
	<form:form id="form_usuario" modelAttribute="usuario" method="post" onsubmit="return false;">
		<div class="nopadding">
			<div class="row">
				<div class="col-md-2 col-xs-2 col-2-label">
					<label for="nome">Nome:</label>
				</div>
				<div class="col-md-10 col-xs-10">
					<form:input path="nome" cssClass="form-control" />
				</div>
			</div>
			<div class="row row-xxheight">
				<div class="col-md-2 col-md-2-xxheigth col-xs-2 col-2-label">
					<label for="veiculos">Veiculos:</label>
				</div>
				<div class="col-md-3 col-xs-3">
					<form:select path="veiculos" cssClass="form-control">
						<form:options items="${veiculos}" itemValue="id" itemLabel="modelo" />
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<a id="bnt_send_form" class="btn btn-primary" href="#">Enviar</a>
				</div>
			</div>
		</div>
	</form:form>
</div>
</div>