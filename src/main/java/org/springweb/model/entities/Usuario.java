/**
 * 
 */
package org.springweb.model.entities;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "usuarios")
@AttributeOverride(name = "id", column = @Column(name = "usu_id"))
public class Usuario extends AbstractEntity {

	@Column(name = "usu_nome")
	private String nome;

	@OneToMany
	@JoinColumn(name = "usu_id")
	private List<Veiculo> veiculos;

	public Usuario() {
	}

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	public Usuario(Long pId, String pNome) {

		setId(pId);
		setNome(pNome);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String pNome) {
		nome = pNome;
	}

}
