/**
 * 
 */
package org.springweb.scan.services;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springweb.model.entities.Usuario;
import org.springweb.model.entities.Veiculo;


public interface IHomeService {
	
	@Transactional
	List<Usuario> getUsuarios();
	
	List<Veiculo> getVeiculos();
	
	@Transactional
	void inserirUsuario(Usuario pUsuario);
	
	@Transactional
	void getUsuarioById(Long pId);

}
