package org.springweb.scan.interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogInterceptor {
	
	
	@Before("execution(* org.springweb.scan.persistence.dao.VeiculoDAO.getAll())")
	public void interceptDaoBefore(JoinPoint pJoinPoint){
		
		log.debug(" *** Interceptou Before [ " + pJoinPoint.toShortString() + " ] *** ");
	}
	
	
	@Around("execution(* org.springweb.scan.persistence.dao.UsuarioDAO.inserir(org.springweb.model.entities.Usuario))")
	public void interceptDaoAround(ProceedingJoinPoint pJoinPoint) throws Throwable{
		
		log.debug(" *** Interceptou Around [ " + pJoinPoint.toShortString() + " ] *** ");
		
		@SuppressWarnings("unused")
		Object[] args = pJoinPoint.getArgs();
		
		log.debug(" *** Interceptou Around [ " + pJoinPoint.toShortString() + " ] *** ");
		pJoinPoint.proceed();
	}
	
	
	
	@After("execution(* org.springweb.scan.persistence.dao.UsuarioDAO.getAll(..))")
	public void interceptDaoAfter(JoinPoint pJoinPoint){
		
		log.debug(" *** Interceptou After [ " + pJoinPoint.toShortString() + " ] *** ");
	}
	
	private static final Log log = LogFactory.getLog(LogInterceptor.class); 
}
