/**
 * 
 */
package org.springweb.scan.persistence.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springweb.model.entities.Usuario;


@Repository
public class UsuarioDAO {

	@PersistenceContext
	EntityManager em;
	
	public List<Usuario> getAll(){
		return em.createQuery("from Usuario u", Usuario.class).getResultList();
	}
	
	public void inserir(Usuario pUsuario){
		em.persist(pUsuario);
	}
	
	public Usuario getById(Long pId){
		return em.find(Usuario.class, pId);
	}
	
	
}
