/**
 * 
 */
package org.springweb.scan.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springweb.model.entities.Usuario;
import org.springweb.scan.services.IHomeService;


@Controller
@RequestMapping("home")
public class HomeController {
	
	@RequestMapping("inicializar")
	public String Home(@ModelAttribute("usuario") Usuario pUsuario, ModelMap pMap){		
				
		pMap.addAttribute("veiculos", homeService.getVeiculos());
				
		return "home";
	}
	
	
	@RequestMapping("send")
	public String sendForm(@ModelAttribute("usuario") Usuario pUsuario){
		
		homeService.inserirUsuario(pUsuario);
				
		return "home";		
	}
	
	
	@Autowired
	private IHomeService homeService;
}
